/*  classique-engine-style.h
 *
 *  This library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _CLASSIQUE_ENGINE_STYLE_H_
#define _CLASSIQUE_ENGINE_STYLE_H_

#include <gtk/gtkstyle.h>
#include "classique-engine.h"

typedef struct _ClassiqueEngineStyle ClassiqueEngineStyle;
typedef struct _ClassiqueEngineStyleClass ClassiqueEngineStyleClass;

extern GType classique_engine_type_style;

#define CLASSIQUE_ENGINE_TYPE_STYLE               classique_engine_type_style
#define CLASSIQUE_ENGINE_STYLE(object)           (G_TYPE_CHECK_INSTANCE_CAST ((object), CLASSIQUE_ENGINE_TYPE_STYLE, ClassiqueEngineStyle))
#define CLASSIQUE_ENGINE_STYLE_CLASS(klass)      (G_TYPE_CHECK_CLASS_CAST ((klass), CLASSIQUE_ENGINE_TYPE_STYLE, ClassiqueEngineStyleClass))
#define CLASSIQUE_ENGINE_IS_STYLE(object)        (G_TYPE_CHECK_INSTANCE_TYPE ((object), CLASSIQUE_ENGINE_TYPE_STYLE))
#define CLASSIQUE_ENGINE_IS_STYLE_CLASS(klass)   (G_TYPE_CHECK_CLASS_TYPE ((klass), CLASSIQUE_ENGINE_TYPE_STYLE))
#define CLASSIQUE_ENGINE_STYLE_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), CLASSIQUE_ENGINE_TYPE_STYLE, ClassiqueEngineStyleClass))

struct _ClassiqueEngineStyle {
	GtkStyle parent_instance;
};

struct _ClassiqueEngineStyleClass {
	GtkStyleClass parent_class;
};

void classique_engine_style_register_type (GTypeModule *module);

#endif
