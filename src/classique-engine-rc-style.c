/*  classique-engine-rc-style.c
 *
 *  This library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "classique-engine.h"
#include "classique-engine-style.h"
#include "classique-engine-rc-style.h"

static void classique_engine_rc_style_class_init (ClassiqueEngineRcStyleClass *klass);
static void classique_engine_rc_style_init (ClassiqueEngineRcStyle *style);
static void classique_engine_rc_style_finalize (GObject *object);
static GtkStyle *classique_engine_rc_style_create_style (GtkRcStyle *rc_style);

static GtkRcStyleClass *parent_class;

GType classique_engine_type_rc_style = 0;

void
classique_engine_rc_style_register_type (GTypeModule *module)
{
	static const GTypeInfo object_info = {
		sizeof (ClassiqueEngineRcStyleClass),
		(GBaseInitFunc) NULL,
		(GBaseFinalizeFunc) NULL,
		(GClassInitFunc) classique_engine_rc_style_class_init,
		NULL, /* class_finalize */
		NULL, /* class_data */
		sizeof (ClassiqueEngineRcStyle),
		0,		/* n_preallocs */
		(GInstanceInitFunc) classique_engine_rc_style_init,
	};

	classique_engine_type_rc_style = g_type_module_register_type (module,
							    GTK_TYPE_RC_STYLE,
							    "ClassiqueEngineRcStyle",
							    &object_info, 0);
}

static void
classique_engine_rc_style_class_init (ClassiqueEngineRcStyleClass *klass)
{
	GtkRcStyleClass *rc_style_class = GTK_RC_STYLE_CLASS (klass);
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);
	object_class->finalize = classique_engine_rc_style_finalize;

	rc_style_class->create_style = classique_engine_rc_style_create_style;
}

static void
classique_engine_rc_style_init (ClassiqueEngineRcStyle *style)
{
}

static void
classique_engine_rc_style_finalize (GObject *object)
{
	ClassiqueEngineRcStyle *mac_rc_style;

	g_return_if_fail (object != NULL);
	g_return_if_fail (CLASSIQUE_ENGINE_IS_RC_STYLE (object));
	mac_rc_style = CLASSIQUE_ENGINE_RC_STYLE (object);

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static GtkStyle *
classique_engine_rc_style_create_style (GtkRcStyle *rc_style)
{
	ClassiqueEngineStyle *mac_style;
	ClassiqueEngineRcStyle *mac_rc_style;

	g_return_if_fail (rc_style != NULL);
	g_return_if_fail (CLASSIQUE_ENGINE_IS_RC_STYLE (rc_style));

	mac_rc_style = CLASSIQUE_ENGINE_RC_STYLE (rc_style);
	mac_style = g_object_new (CLASSIQUE_ENGINE_TYPE_STYLE, NULL);

	return GTK_STYLE (mac_style);
}
