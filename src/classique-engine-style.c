/*  classique-engine-style.c
 *
 *  This library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Library General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <math.h>

#include "classique-engine.h"
#include "classique-engine-rc-style.h"
#include "classique-engine-style.h"

#define DETAIL(xx) ((detail) && (!strcmp(xx, detail)))

#ifndef M_PI
#define M_PI    3.14159265358979323846
#endif /* M_PI */
#ifndef M_PI_4
#define M_PI_4  0.78539816339744830962
#endif /* M_PI_4 */

static void classique_engine_style_class_init (ClassiqueEngineStyleClass *klass);
static void classique_engine_style_init (ClassiqueEngineStyle *mac_style);
static void classique_engine_style_finalize (GObject *object);

static void draw_hline (GtkStyle *style,
			GdkWindow *window,
			GtkStateType state_type,
			GdkRectangle *area,
			GtkWidget *widget, const gchar *detail, gint x1, gint x2, gint y);
static void draw_vline (GtkStyle *style,
			GdkWindow *window,
			GtkStateType state_type,
			GdkRectangle *area,
			GtkWidget *widget, const gchar *detail, gint y1, gint y2, gint x);
static void draw_shadow (GtkStyle *style,
			 GdkWindow *window,
			 GtkStateType state_type,
			 GtkShadowType shadow_type,
			 GdkRectangle *area,
			 GtkWidget *widget,
			 const gchar *detail, gint x, gint y, gint width, gint height);
static void draw_polygon (GtkStyle *style,
			  GdkWindow *window,
			  GtkStateType state_type,
			  GtkShadowType shadow_type,
			  GdkRectangle *area,
			  GtkWidget *widget,
			  const gchar *detail, GdkPoint *point, gint npoints, gint fill);
static void draw_arrow (GtkStyle *style,
			GdkWindow *window,
			GtkStateType state_type,
			GtkShadowType shadow_type,
			GdkRectangle *area,
			GtkWidget *widget,
			const gchar *detail,
			GtkArrowType arrow_type,
			gint fill, gint x, gint y, gint width, gint height);
static void draw_diamond (GtkStyle *style,
			  GdkWindow *window,
			  GtkStateType state_type,
			  GtkShadowType shadow_type,
			  GdkRectangle *area,
			  GtkWidget *widget,
			  const gchar *detail, gint x, gint y, gint width, gint height);
static void draw_box (GtkStyle *style,
		      GdkWindow *window,
		      GtkStateType state_type,
		      GtkShadowType shadow_type,
		      GdkRectangle *area,
		      GtkWidget *widget,
		      const gchar *detail, gint x, gint y, gint width, gint height);
static void draw_flat_box (GtkStyle *style,
			   GdkWindow *window,
			   GtkStateType state_type,
			   GtkShadowType shadow_type,
			   GdkRectangle *area,
			   GtkWidget *widget,
			   const gchar *detail, gint x, gint y, gint width, gint height);
static void draw_check (GtkStyle *style,
			GdkWindow *window,
			GtkStateType state_type,
			GtkShadowType shadow_type,
			GdkRectangle *area,
			GtkWidget *widget,
			const gchar *detail, gint x, gint y, gint width, gint height);
static void draw_option (GtkStyle *style,
			 GdkWindow *window,
			 GtkStateType state_type,
			 GtkShadowType shadow_type,
			 GdkRectangle *area,
			 GtkWidget *widget,
			 const gchar *detail, gint x, gint y, gint width, gint height);
static void draw_tab (GtkStyle *style,
		      GdkWindow *window,
		      GtkStateType state_type,
		      GtkShadowType shadow_type,
		      GdkRectangle *area,
		      GtkWidget *widget,
		      const gchar *detail, gint x, gint y, gint width, gint height);
static void draw_shadow_gap (GtkStyle *style,
			     GdkWindow *window,
			     GtkStateType state_type,
			     GtkShadowType shadow_type,
			     GdkRectangle *area,
			     GtkWidget *widget,
			     const gchar *detail,
			     gint x,
			     gint y,
			     gint width,
			     gint height, GtkPositionType gap_side, gint gap_x, gint gap_width);
static void draw_box_gap (GtkStyle *style,
			  GdkWindow *window,
			  GtkStateType state_type,
			  GtkShadowType shadow_type,
			  GdkRectangle *area,
			  GtkWidget *widget,
			  const gchar *detail,
			  gint x,
			  gint y,
			  gint width,
			  gint height, GtkPositionType gap_side, gint gap_x, gint gap_width);
static void draw_extension (GtkStyle *style,
			    GdkWindow *window,
			    GtkStateType state_type,
			    GtkShadowType shadow_type,
			    GdkRectangle *area,
			    GtkWidget *widget,
			    const gchar *detail,
			    gint x, gint y, gint width, gint height, GtkPositionType gap_side);
static void draw_focus (GtkStyle *style,
			GdkWindow *window,
			GtkStateType state_type,
			GdkRectangle *area,
			GtkWidget *widget,
			const gchar *detail, gint x, gint y, gint width, gint height);
static void draw_slider (GtkStyle *style,
			 GdkWindow *window,
			 GtkStateType state_type,
			 GtkShadowType shadow_type,
			 GdkRectangle *area,
			 GtkWidget *widget,
			 const gchar *detail,
			 gint x, gint y, gint width, gint height, GtkOrientation orientation);
static void draw_handle (GtkStyle *style,
			 GdkWindow *window,
			 GtkStateType state_type,
			 GtkShadowType shadow_type,
			 GdkRectangle *area,
			 GtkWidget *widget,
			 const gchar *detail,
			 gint x, gint y, gint width, gint height, GtkOrientation orientation);

static void classique_engine_style_shade (GdkColor *a, GdkColor *b, gdouble k);
static void hls_to_rgb (gdouble *h, gdouble *l, gdouble *s);
static void rgb_to_hls (gdouble *r, gdouble *g, gdouble *b);

static GtkStyleClass *parent_class = NULL;

GType classique_engine_type_style = 0;

void
classique_engine_style_register_type (GTypeModule *module)
{
	static const GTypeInfo object_info = {
		sizeof (ClassiqueEngineStyleClass),
		(GBaseInitFunc) NULL,
		(GBaseFinalizeFunc) NULL,
		(GClassInitFunc) classique_engine_style_class_init,
		NULL, /* class_finalize */
		NULL, /* class_data */
		sizeof (ClassiqueEngineStyle),
		0,		/* n_preallocs */
		(GInstanceInitFunc) classique_engine_style_init,
	};

	classique_engine_type_style = g_type_module_register_type (module,
							     GTK_TYPE_STYLE,
							     "ClassiqueEngineStyle",
							     &object_info, 0);
}

static void
classique_engine_style_class_init (ClassiqueEngineStyleClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	GtkStyleClass *style_class = GTK_STYLE_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);
	object_class->finalize = classique_engine_style_finalize;

	/* Drawing over-rides */
	style_class->draw_hline = draw_hline;
	style_class->draw_vline = draw_vline;
	style_class->draw_shadow = draw_shadow;
	style_class->draw_polygon = draw_polygon;
	style_class->draw_arrow = draw_arrow;
	style_class->draw_diamond = draw_diamond;
	/*style_class->draw_oval = draw_oval; */
	/*style_class->draw_string = draw_string; */
	style_class->draw_box = draw_box;
	style_class->draw_flat_box = draw_flat_box;
	style_class->draw_check = draw_check;
	style_class->draw_option = draw_option;
	/*style_class->draw_cross = draw_cross; */
	/*style_class->draw_ramp = draw_ramp; */
	style_class->draw_tab = draw_tab;
	style_class->draw_shadow_gap = draw_shadow_gap;
	style_class->draw_box_gap = draw_box_gap;
	style_class->draw_extension = draw_extension;
	style_class->draw_focus = draw_focus;
	style_class->draw_slider = draw_slider;
	style_class->draw_handle = draw_handle;
}

static void
classique_engine_style_init (ClassiqueEngineStyle *mac_style)
{
}

static void
classique_engine_style_finalize (GObject *object)
{
	ClassiqueEngineStyle *mac_style;

	mac_style = CLASSIQUE_ENGINE_STYLE (object);

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

/* And now we make it happen */

static void
draw_hline (GtkStyle *style,
	    GdkWindow *window,
	    GtkStateType state_type,
	    GdkRectangle *area, GtkWidget *widget, const gchar *detail, gint x1, gint x2, gint y)
{
	ClassiqueEngineStyle *mac_style = CLASSIQUE_ENGINE_STYLE (style);
	gint thickness_light;
	gint thickness_dark;
	gint i;
	GdkGC *gc1 = NULL;
	GdkGC *gc2 = NULL;

	g_return_if_fail (style != NULL);
	g_return_if_fail (window != NULL);

	thickness_light = style->ythickness / 2;
	thickness_dark = style->ythickness - thickness_light;

	if (DETAIL ("menuitem"))
		y++;

	gc1 = style->light_gc[state_type];
	gc2 = style->dark_gc[state_type];

	if (area) {
		gdk_gc_set_clip_rectangle (gc1, area);
		gdk_gc_set_clip_rectangle (gc2, area);
	}
	for (i = 0; i < thickness_dark; i++) {
		gdk_draw_line (window, gc2, x1, y + i, x2 - i - 1, y + i);
	}

	y += thickness_dark;
	for (i = 0; i < thickness_light; i++) {
		gdk_draw_line (window, gc2, x1, y + i, x1 + thickness_light - i - 1, y + i);
		gdk_draw_line (window, gc1, x1 + thickness_light - i - 1, y + i, x2, y + i);
	}
	if (area) {
		gdk_gc_set_clip_rectangle (gc1, NULL);
		gdk_gc_set_clip_rectangle (gc2, NULL);
	}
}

static void
draw_vline (GtkStyle *style,
	    GdkWindow *window,
	    GtkStateType state_type,
	    GdkRectangle *area, GtkWidget *widget, const gchar *detail, gint y1, gint y2, gint x)
{
	ClassiqueEngineStyle *mac_style = CLASSIQUE_ENGINE_STYLE (style);
	gint thickness_light;
	gint thickness_dark;
	gint i;
	GdkGC *gc1 = NULL;
	GdkGC *gc2 = NULL;

	g_return_if_fail (style != NULL);
	g_return_if_fail (window != NULL);

	thickness_light = style->xthickness / 2;
	thickness_dark = style->xthickness - thickness_light;

	gc1 = style->light_gc[state_type];
	gc2 = style->dark_gc[state_type];

	if (area) {
		gdk_gc_set_clip_rectangle (gc1, area);
		gdk_gc_set_clip_rectangle (gc2, area);
	}
	for (i = 0; i < thickness_dark; i++) {
		gdk_draw_line (window, gc1, x + i, y2 - i - 1, x + i, y2);
		gdk_draw_line (window, gc2, x + i, y1, x + i, y2 - i - 1);
	}

	x += thickness_dark;
	for (i = 0; i < thickness_light; i++) {
		gdk_draw_line (window, gc2, x + i, y1, x + i, y1 + thickness_light - i);
		gdk_draw_line (window, gc1, x + i, y1 + thickness_light - i, x + i, y2);
	}
	if (area) {
		gdk_gc_set_clip_rectangle (gc1, NULL);
		gdk_gc_set_clip_rectangle (gc2, NULL);
	}
}

static void
draw_shadow (GtkStyle *style,
	     GdkWindow *window,
	     GtkStateType state_type,
	     GtkShadowType shadow_type,
	     GdkRectangle *area,
	     GtkWidget *widget, const gchar *detail, gint x, gint y, gint width, gint height)
{
	ClassiqueEngineStyle *mac_style = CLASSIQUE_ENGINE_STYLE (style);
	GdkGC *light = NULL;
	GdkGC *dark = NULL;
	GdkGC *mid = NULL;

	g_return_if_fail (style != NULL);
	g_return_if_fail (window != NULL);

	if ((width == -1) && (height == -1))
		gdk_window_get_size (window, &width, &height);
	else if (width == -1)
		gdk_window_get_size (window, &width, NULL);
	else if (height == -1)
		gdk_window_get_size (window, NULL, &height);

	if (shadow_type == GTK_SHADOW_NONE)
		return;

	dark = style->dark_gc[state_type];
	light = style->light_gc[state_type];
	mid = style->mid_gc[state_type];

	if (area) {
		gdk_gc_set_clip_rectangle (dark, area);
		gdk_gc_set_clip_rectangle (light, area);
		gdk_gc_set_clip_rectangle (mid, area);
		gdk_gc_set_clip_rectangle (style->black_gc, area);
		gdk_gc_set_clip_rectangle (style->white_gc, area);
	}

	if (DETAIL ("entry")) {
		gdk_draw_rectangle (window, style->base_gc[GTK_STATE_NORMAL],
				    TRUE, x, y, width, height);
	}

	switch (shadow_type) {
	case GTK_SHADOW_NONE:
		break;
	case GTK_SHADOW_IN:
		/* Outside  Left*/
		gdk_draw_line (window, style->black_gc, x, y + 1, x, y + height - 2);
		/* Outside Top */
		gdk_draw_line (window, style->black_gc, x + 1, y, x + width - 2, y);
		/* Outside Right */
		gdk_draw_line (window, light, x + width - 1, y + 1, x + width - 1, y + height - 2);
		/* Outside Bottom */
		gdk_draw_line (window, light, x + 1, y + height - 1, x + width - 2, y + height - 1);
		/* Inside  Left */
		gdk_draw_line (window, mid, x + 1, y + 1, x + 1, y + height - 2);
		/* Inside  Top */
		gdk_draw_line (window, mid, x + 1, y + 1, x + width - 2, y + 1);
		/* Inside Right */
		gdk_draw_line (window, dark, x + width - 2, y + 1, x + width - 2, y + height - 2);
		/* Inside Bottom */
		gdk_draw_line (window, dark, x + 1, y + height - 2, x + width - 2, y + height - 2);
		break;
	case GTK_SHADOW_ETCHED_IN:
		/* Outside  Left*/
		gdk_draw_line (window, style->black_gc, x, y, x, y + height - 1);
		/* Outside Top */
		gdk_draw_line (window, style->black_gc, x, y, x + width - 1, y);
		/* Outside Right */
		gdk_draw_line (window, light, x + width - 1, y, x + width - 1, y + height - 1);
		/* Outside Bottom */
		gdk_draw_line (window, light, x, y + height - 1, x + width - 1, y + height - 1);
		/* Inside  Left */
		gdk_draw_line (window, mid, x + 1, y + 1, x + 1, y + height - 2);
		/* Inside  Top */
		gdk_draw_line (window, mid, x + 1, y + 1, x + width - 2, y + 1);
		/* Inside Right */
		gdk_draw_line (window, dark, x + width - 2, y + 1, x + width - 2, y + height - 2);
		/* Inside Bottom */
		gdk_draw_line (window, dark, x + 1, y + height - 2, x + width - 2, y + height - 2);
		break;
	case GTK_SHADOW_OUT:
		/* Outside  Left*/
		gdk_draw_line (window, dark, x, y + 1, x, y + height - 2);
		/* Outside Top */
		gdk_draw_line (window, dark, x + 1, y, x + width - 2, y);
		/* Outside Right */
		gdk_draw_line (window, style->black_gc, x + width - 1, y + 1, x + width - 1, y + height - 2);
		/* Outside Bottom */
		gdk_draw_line (window, style->black_gc, x + 1, y + height - 1, x + width - 2, y + height - 1);
		/* Inside  Left */
		gdk_draw_line (window, light, x + 1, y + 1, x + 1, y + height - 2);
		/* Inside  Top */
		gdk_draw_line (window, light, x + 1, y + 1, x + width - 2, y + 1);
		/* Inside Right */
		gdk_draw_line (window, mid, x + width - 2, y + 1, x + width - 2, y + height - 2);
		/* Inside Bottom */
		gdk_draw_line (window, mid, x + 1, y + height - 2, x + width - 2, y + height - 2);
		break;
	case GTK_SHADOW_ETCHED_OUT:
		/* Outside  Left*/
		gdk_draw_line (window, dark, x, y, x, y + height);
		/* Outside Top */
		gdk_draw_line (window, dark, x, y, x + width, y);
		/* Outside Right */
		gdk_draw_line (window, style->black_gc, x + width - 1, y, x + width - 1, y + height);
		/* Outside Bottom */
		gdk_draw_line (window, style->black_gc, x, y + height - 1, x + width, y + height - 1);
		/* Inside  Left */
		gdk_draw_line (window, light, x + 1, y + 1, x + 1, y + height - 2);
		/* Inside  Top */
		gdk_draw_line (window, light, x + 1, y + 1, x + width - 2, y + 1);
		/* Inside Right */
		gdk_draw_line (window, mid, x + width - 2, y + 1, x + width - 2, y + height - 2);
		/* Inside Bottom */
		gdk_draw_line (window, mid, x + 1, y + height - 2, x + width - 2, y + height - 2);
		break;
	}

	if (area) {
		gdk_gc_set_clip_rectangle (light, NULL);
		gdk_gc_set_clip_rectangle (dark, NULL);
		gdk_gc_set_clip_rectangle (mid, NULL);
		gdk_gc_set_clip_rectangle (style->black_gc, NULL);
		gdk_gc_set_clip_rectangle (style->white_gc, NULL);
	}
}

static void
draw_polygon (GtkStyle *style,
	      GdkWindow *window,
	      GtkStateType state_type,
	      GtkShadowType shadow_type,
	      GdkRectangle *area,
	      GtkWidget *widget, const gchar *detail, GdkPoint *points, gint npoints, gint fill)
{
	static const gdouble pi_over_4 = M_PI_4;
	static const gdouble pi_3_over_4 = M_PI_4 *3;

	GdkGC *gc1;
	GdkGC *gc2;
	GdkGC *gc3;
	GdkGC *gc4;
	gdouble angle;
	gint xadjust;
	gint yadjust;
	gint i;

	g_return_if_fail (style != NULL);
	g_return_if_fail (window != NULL);
	g_return_if_fail (points != NULL);

	switch (shadow_type) {
	case GTK_SHADOW_IN:
		gc1 = style->light_gc[state_type];
		gc2 = style->dark_gc[state_type];
		gc3 = style->light_gc[state_type];
		gc4 = style->dark_gc[state_type];
		break;
	case GTK_SHADOW_ETCHED_IN:
		gc1 = style->light_gc[state_type];
		gc2 = style->dark_gc[state_type];
		gc3 = style->dark_gc[state_type];
		gc4 = style->light_gc[state_type];
		break;
	case GTK_SHADOW_OUT:
		gc1 = style->dark_gc[state_type];
		gc2 = style->light_gc[state_type];
		gc3 = style->dark_gc[state_type];
		gc4 = style->light_gc[state_type];
		break;
	case GTK_SHADOW_ETCHED_OUT:
		gc1 = style->dark_gc[state_type];
		gc2 = style->light_gc[state_type];
		gc3 = style->light_gc[state_type];
		gc4 = style->dark_gc[state_type];
		break;
	default:
		return;
	}

	if (area) {
		gdk_gc_set_clip_rectangle (gc1, area);
		gdk_gc_set_clip_rectangle (gc2, area);
		gdk_gc_set_clip_rectangle (gc3, area);
		gdk_gc_set_clip_rectangle (gc4, area);
	}

	if (fill)
		gdk_draw_polygon (window, style->bg_gc[state_type], TRUE, points, npoints);

	npoints--;

	for (i = 0; i < npoints; i++) {
		if ((points[i].x == points[i + 1].x) && (points[i].y == points[i + 1].y)) {
			angle = 0;
		} else {
			angle = atan2 (points[i + 1].y - points[i].y,
				       points[i + 1].x - points[i].x);
		}

		if ((angle > -pi_3_over_4) && (angle < pi_over_4)) {
			if (angle > -pi_over_4) {
				xadjust = 0;
				yadjust = 1;
			} else {
				xadjust = 1;
				yadjust = 0;
			}

			gdk_draw_line (window, gc1,
				       points[i].x - xadjust,
				       points[i].y - yadjust,
				       points[i + 1].x - xadjust, points[i + 1].y - yadjust);
			gdk_draw_line (window, gc3, points[i].x, points[i].y,
				       points[i + 1].x, points[i + 1].y);
		} else {
			if ((angle < -pi_3_over_4) || (angle > pi_3_over_4)) {
				xadjust = 0;
				yadjust = 1;
			} else {
				xadjust = 1;
				yadjust = 0;
			}

			gdk_draw_line (window, gc4,
				       points[i].x + xadjust,
				       points[i].y + yadjust,
				       points[i + 1].x + xadjust, points[i + 1].y + yadjust);
			gdk_draw_line (window, gc2, points[i].x, points[i].y,
				       points[i + 1].x, points[i + 1].y);
		}
	}
	if (area) {
		gdk_gc_set_clip_rectangle (gc1, NULL);
		gdk_gc_set_clip_rectangle (gc2, NULL);
		gdk_gc_set_clip_rectangle (gc3, NULL);
		gdk_gc_set_clip_rectangle (gc4, NULL);
	}
}

static void
draw_arrow (GtkStyle *style,
	    GdkWindow *window,
	    GtkStateType state_type,
	    GtkShadowType shadow_type,
	    GdkRectangle *area,
	    GtkWidget *widget,
	    const gchar *detail,
	    GtkArrowType arrow_type, gint fill, gint x, gint y, gint width, gint height)
{
	GdkGC *gc1;
	GdkGC *gc2;

	GdkPoint points[3];

	g_return_if_fail (style != NULL);
	g_return_if_fail (window != NULL);

	if ((width == -1) && (height == -1))
		gdk_window_get_size (window, &width, &height);
	else if (width == -1)
		gdk_window_get_size (window, &width, NULL);
	else if (height == -1)
		gdk_window_get_size (window, NULL, &height);

	if (DETAIL ("menuitem")) {
		points[0].x = x;
		points[0].y = y + 1;
		points[1].x = x + (height - 2) / 2;
		points[1].y = y + height / 2;
		points[2].x = x;
		points[2].y = y + height - 1;
	} else {
		if (DETAIL ("vscrollbar") || DETAIL ("hscrollbar"))
		{
			gtk_paint_box (style, window, state_type, shadow_type,
				       area, widget, detail, x, y, width, height - 1);
			x += 1;
			y += 1;
			width -= 2;
			height -= 2;
		}
		switch (arrow_type) {
		case GTK_ARROW_UP:
			points[0].x = x + 2;
			points[0].y = (y + height - 1) - (height / 3);
			points[1].x = points[0].x + ((width - 4) / 2);
			points[1].y = points[0].y - ((width - 4) / 2);
			points[2].x = points[1].x + ((width - 4) / 2);
			points[2].y = points[1].y + ((width - 4) / 2);
			break;

		case GTK_ARROW_DOWN:
			points[0].x = x + 2;
			points[0].y = y + (height / 3);
			points[1].x = points[0].x + ((width - 4) / 2);
			points[1].y = points[0].y + ((width - 4) / 2);
			points[2].x = points[1].x + ((width - 4) / 2);
			points[2].y = points[1].y - ((width - 4) / 2);
			break;

		case GTK_ARROW_LEFT:
			points[0].y = y + 2;
			points[0].x = (x + width - 1) - (width / 3);
			points[1].y = points[0].y + ((height - 4) / 2);
			points[1].x = points[0].x - ((height - 4) / 2);
			points[2].y = points[1].y + ((height - 4) / 2);
			points[2].x = points[1].x + ((height - 4) / 2);
			break;

		case GTK_ARROW_RIGHT:
			points[0].y = y + 2;
			points[0].x = x + (width / 3);
			points[1].y = points[0].y + ((height - 4) / 2);
			points[1].x = points[0].x + ((height - 4) / 2);
			points[2].y = points[1].y + ((height - 4) / 2);
			points[2].x = points[1].x - ((height - 4) / 2);
			break;
		}
	}
	gdk_draw_polygon (window, style->fg_gc[state_type], FALSE, points, 3);
	gdk_draw_polygon (window, style->fg_gc[state_type], TRUE, points, 3);
}

static void
draw_diamond (GtkStyle *style,
	      GdkWindow *window,
	      GtkStateType state_type,
	      GtkShadowType shadow_type,
	      GdkRectangle *area,
	      GtkWidget *widget, const gchar *detail, gint x, gint y, gint width, gint height)
{
	gint half_width;
	gint half_height;

	g_return_if_fail (style != NULL);
	g_return_if_fail (window != NULL);

	if ((width == -1) && (height == -1))
		gdk_window_get_size (window, &width, &height);
	else if (width == -1)
		gdk_window_get_size (window, &width, NULL);
	else if (height == -1)
		gdk_window_get_size (window, NULL, &height);

	half_width = width / 2;
	half_height = height / 2;

	if (area) {
		gdk_gc_set_clip_rectangle (style->light_gc[state_type], area);
		gdk_gc_set_clip_rectangle (style->bg_gc[state_type], area);
		gdk_gc_set_clip_rectangle (style->dark_gc[state_type], area);
		gdk_gc_set_clip_rectangle (style->black_gc, area);
	}

	switch (shadow_type) {
	case GTK_SHADOW_IN:
		gdk_draw_line (window, style->light_gc[state_type],
			       x + 2, y + half_height, x + half_width, y + height - 2);
		gdk_draw_line (window, style->light_gc[state_type],
			       x + half_width, y + height - 2, x + width - 2, y + half_height);
		gdk_draw_line (window, style->light_gc[state_type],
			       x + 1, y + half_height, x + half_width, y + height - 1);
		gdk_draw_line (window, style->light_gc[state_type],
			       x + half_width, y + height - 1, x + width - 1, y + half_height);
		gdk_draw_line (window, style->light_gc[state_type],
			       x, y + half_height, x + half_width, y + height);
		gdk_draw_line (window, style->light_gc[state_type],
			       x + half_width, y + height, x + width, y + half_height);

		gdk_draw_line (window, style->dark_gc[state_type],
			       x + 2, y + half_height, x + half_width, y + 2);
		gdk_draw_line (window, style->dark_gc[state_type],
			       x + half_width, y + 2, x + width - 2, y + half_height);
		gdk_draw_line (window, style->dark_gc[state_type],
			       x + 1, y + half_height, x + half_width, y + 1);
		gdk_draw_line (window, style->dark_gc[state_type],
			       x + half_width, y + 1, x + width - 1, y + half_height);
		gdk_draw_line (window, style->dark_gc[state_type],
			       x, y + half_height, x + half_width, y);
		gdk_draw_line (window, style->dark_gc[state_type],
			       x + half_width, y, x + width, y + half_height);
		break;
	case GTK_SHADOW_OUT:
		gdk_draw_line (window, style->dark_gc[state_type],
			       x + 2, y + half_height, x + half_width, y + height - 2);
		gdk_draw_line (window, style->dark_gc[state_type],
			       x + half_width, y + height - 2, x + width - 2, y + half_height);
		gdk_draw_line (window, style->dark_gc[state_type],
			       x + 1, y + half_height, x + half_width, y + height - 1);
		gdk_draw_line (window, style->dark_gc[state_type],
			       x + half_width, y + height - 1, x + width - 1, y + half_height);
		gdk_draw_line (window, style->dark_gc[state_type],
			       x, y + half_height, x + half_width, y + height);
		gdk_draw_line (window, style->dark_gc[state_type],
			       x + half_width, y + height, x + width, y + half_height);

		gdk_draw_line (window, style->light_gc[state_type],
			       x + 2, y + half_height, x + half_width, y + 2);
		gdk_draw_line (window, style->light_gc[state_type],
			       x + half_width, y + 2, x + width - 2, y + half_height);
		gdk_draw_line (window, style->light_gc[state_type],
			       x + 1, y + half_height, x + half_width, y + 1);
		gdk_draw_line (window, style->light_gc[state_type],
			       x + half_width, y + 1, x + width - 1, y + half_height);
		gdk_draw_line (window, style->light_gc[state_type],
			       x, y + half_height, x + half_width, y);
		gdk_draw_line (window, style->light_gc[state_type],
			       x + half_width, y, x + width, y + half_height);
		break;
	default:
		break;
	}

	if (area) {
		gdk_gc_set_clip_rectangle (style->light_gc[state_type], NULL);
		gdk_gc_set_clip_rectangle (style->bg_gc[state_type], NULL);
		gdk_gc_set_clip_rectangle (style->dark_gc[state_type], NULL);
		gdk_gc_set_clip_rectangle (style->black_gc, NULL);
	}
}

static void
draw_box (GtkStyle *style,
	  GdkWindow *window,
	  GtkStateType state_type,
	  GtkShadowType shadow_type,
	  GdkRectangle *area,
	  GtkWidget *widget, const gchar *detail, gint x, gint y, gint width, gint height)
{
	ClassiqueEngineStyle *mac_style = CLASSIQUE_ENGINE_STYLE (style);
	GdkGC *light_gc;
	GdkGC *dark_gc;

	g_return_if_fail (style != NULL);
	g_return_if_fail (window != NULL);

	if ((width == -1) && (height == -1))
		gdk_window_get_size (window, &width, &height);
	else if (width == -1)
		gdk_window_get_size (window, &width, NULL);
	else if (height == -1)
		gdk_window_get_size (window, NULL, &height);

	light_gc = style->light_gc[state_type];
	dark_gc = style->dark_gc[state_type];

	if (DETAIL ("menuitem")) {
		if ((!style->bg_pixmap[state_type]) || (GDK_IS_PIXMAP (window))) {
			if (area) {
				gdk_gc_set_clip_rectangle (style->bg_gc[state_type], area);
			}
			gdk_draw_rectangle (window, style->bg_gc[state_type], TRUE,
					    x + 1, y, width - 3, height - 1);
			gdk_draw_rectangle (window, style->bg_gc[state_type], TRUE,
					    x, y + 1, width - 1, height - 2);
			if (area) {
				gdk_gc_set_clip_rectangle (style->bg_gc[state_type], NULL);
			}
		} else {
			gtk_style_apply_default_background (style, window,
							    widget && !GTK_WIDGET_NO_WINDOW (widget),
							    state_type, area,
							    x + 1, y, width - 3, height - 1);
			gtk_style_apply_default_background (style, window,
							    widget && !GTK_WIDGET_NO_WINDOW (widget),
							    state_type, area,
							    x, y + 1, width - 1, height - 2);
		}
	} else {

		if ((!style->bg_pixmap[state_type]) || (GDK_IS_PIXMAP (window))) {
			if (area) {
				gdk_gc_set_clip_rectangle (style->bg_gc[state_type], area);
			}
			gdk_draw_rectangle (window, style->bg_gc[state_type], TRUE,
					    x + 1, y + 1, width - 2, height - 2);
			if (area) {
				gdk_gc_set_clip_rectangle (style->bg_gc[state_type], NULL);
			}
		} else {
			gtk_style_apply_default_background (style, window,
							    widget && !GTK_WIDGET_NO_WINDOW (widget),
							    state_type, area,
							    x + 1, y + 1, width - 2, height - 2);
		}
		gtk_paint_shadow (style, window, state_type, shadow_type, area,
				  widget, detail, x, y, width, height);
	}

	if (DETAIL ("slider")) {
		if (width > height) {
			draw_vline (style, window, state_type, area, widget,
				    detail, style->ythickness,
				    height - style->ythickness - 1, (width / 2) - 4);
			draw_vline (style, window, state_type, area, widget,
				    detail, style->ythickness,
				    height - style->ythickness - 1, (width / 2) - 1);
			draw_vline (style, window, state_type, area, widget,
				    detail, style->ythickness,
				    height - style->ythickness - 1, (width / 2) + 2);
		} else {
			draw_hline (style, window, state_type, area, widget,
				    detail, style->xthickness,
				    width - style->xthickness - 1, (height / 2) - 4);
			draw_hline (style, window, state_type, area, widget,
				    detail, style->xthickness,
				    width - style->xthickness - 1, (height / 2) - 1);
			draw_hline (style, window, state_type, area, widget,
				    detail, style->xthickness,
				    width - style->xthickness - 1, (height / 2) + 2);
		}
	}
}


static void
draw_flat_box (GtkStyle *style,
	       GdkWindow *window,
	       GtkStateType state_type,
	       GtkShadowType shadow_type,
	       GdkRectangle *area,
	       GtkWidget *widget, const gchar *detail, gint x, gint y, gint width, gint height)
{
	GdkGC *gc1;

	g_return_if_fail (style != NULL);
	g_return_if_fail (window != NULL);

	if ((width == -1) && (height == -1))
		gdk_window_get_size (window, &width, &height);
	else if (width == -1)
		gdk_window_get_size (window, &width, NULL);
	else if (height == -1)
		gdk_window_get_size (window, NULL, &height);

	gc1 = style->bg_gc[state_type];

	if (area) {
		gdk_gc_set_clip_rectangle (gc1, area);
	}

	if (DETAIL ("entry_bg")) {
		gdk_draw_rectangle (window, style->base_gc[GTK_STATE_NORMAL],
				    TRUE, x, y, width, height);
	} else {
		gtk_style_apply_default_background (style, window,
						    widget &&
						    !GTK_WIDGET_NO_WINDOW
						    (widget), state_type, area,
						    x, y, width, height);
	}
	if (DETAIL ("tooltip"))
		gdk_draw_rectangle (window, style->black_gc, FALSE, x, y, width - 1, height - 1);

	if (area) {
		gdk_gc_set_clip_rectangle (gc1, NULL);
	}
}

static void
draw_check (GtkStyle *style,
	    GdkWindow *window,
	    GtkStateType state_type,
	    GtkShadowType shadow_type,
	    GdkRectangle *area,
	    GtkWidget *widget, const gchar *detail, gint x, gint y, gint width, gint height)
{
	if (shadow_type == GTK_SHADOW_IN) {
		gtk_paint_box (style, window, GTK_STATE_ACTIVE, shadow_type,
			       area, widget, detail, x + 1, y + 1, width, height);
		gdk_draw_line (window, style->fg_gc[GTK_STATE_ACTIVE], x + 4,
			       y + height - 4, x + 3, y + (height / 2) - 3);
		gdk_draw_line (window, style->fg_gc[GTK_STATE_ACTIVE], x + 4,
			       y + height - 3, x + 4, y + (height / 2) - 3);
		gdk_draw_line (window, style->fg_gc[GTK_STATE_ACTIVE], x + 4,
			       y + height - 3, x + height - 1, y);
		gdk_draw_line (window, style->fg_gc[GTK_STATE_ACTIVE], x + 4,
			       y + height - 3, x + height, y);
	} else {
		gtk_paint_box (style, window, GTK_STATE_NORMAL, shadow_type,
			       area, widget, detail, x + 1, y + 1, width, height);
	}
}

static void
draw_option (GtkStyle *style,
	     GdkWindow *window,
	     GtkStateType state_type,
	     GtkShadowType shadow_type,
	     GdkRectangle *area,
	     GtkWidget *widget, const gchar *detail, gint x, gint y, gint width, gint height)
{
	ClassiqueEngineStyle *mac_style = CLASSIQUE_ENGINE_STYLE (style);
	GdkGC *gc1;
	GdkGC *gc2;
	GdkGC *gc3;

	if (shadow_type == GTK_SHADOW_IN) {
		gc1 = style->light_gc[state_type];
		gc2 = style->dark_gc[state_type];
		gc3 = style->bg_gc[GTK_STATE_ACTIVE];
	} else {
		gc1 = style->dark_gc[state_type];
		gc2 = style->light_gc[state_type];
		gc3 = style->bg_gc[GTK_STATE_NORMAL];
	}

	if (area) {
		gdk_gc_set_clip_rectangle (gc1, area);
		gdk_gc_set_clip_rectangle (gc2, area);
		gdk_gc_set_clip_rectangle (gc3, area);
	}

	gdk_draw_arc (window, gc3, TRUE, x, y, width, height, 0, 360 *64);
	gdk_draw_arc (window, style->dark_gc[state_type], FALSE,
		      x - 1, y - 1, width + 2, height + 2, 0, 360 *64);
	gdk_draw_arc (window, gc1, FALSE, x, y, width, height, 45 *64, 225 *64);
	gdk_draw_arc (window, gc2, FALSE, x, y, width, height, 225 *64, 180 *64);

	if (shadow_type == GTK_SHADOW_IN) {
		gdk_draw_arc (window, style->fg_gc[GTK_STATE_ACTIVE], TRUE,
			      x + 2, y + 2, width - 4, height - 4, 0, 360 *64);
		gdk_draw_arc (window, style->fg_gc[GTK_STATE_ACTIVE], FALSE,
			      x + 2, y + 2, width - 4, height - 4, 0, 360 *64);
	}

	if (area) {
		gdk_gc_set_clip_rectangle (gc1, NULL);
		gdk_gc_set_clip_rectangle (gc2, NULL);
		gdk_gc_set_clip_rectangle (gc3, NULL);
	}
}

static void
draw_tab (GtkStyle *style,
	  GdkWindow *window,
	  GtkStateType state_type,
	  GtkShadowType shadow_type,
	  GdkRectangle *area,
	  GtkWidget *widget, const gchar *detail, gint x, gint y, gint width, gint height)
{
	GdkPoint points[3];

	g_return_if_fail (style != NULL);
	g_return_if_fail (window != NULL);

	draw_arrow (style, window, state_type, GTK_SHADOW_NONE, 
		    area, widget, detail, GTK_ARROW_UP, TRUE,
		    x - 2, y - 2, width + 4, height);
	draw_arrow (style, window, state_type, GTK_SHADOW_NONE, 
		    area, widget, detail, GTK_ARROW_DOWN, TRUE,
		    x - 2, y + (height / 3), width + 4, height);
}

static void
draw_shadow_gap (GtkStyle *style,
		 GdkWindow *window,
		 GtkStateType state_type,
		 GtkShadowType shadow_type,
		 GdkRectangle *area,
		 GtkWidget *widget,
		 const gchar *detail,
		 gint x,
		 gint y,
		 gint width, gint height, GtkPositionType gap_side, gint gap_x, gint gap_width)
{
	GdkRectangle rect;

	g_return_if_fail (style != NULL);
	g_return_if_fail (window != NULL);

	gtk_paint_shadow (style, window, state_type, shadow_type, area, widget,
			  detail, x, y, width, height);

	switch (gap_side) {
	case GTK_POS_TOP:
		rect.x = x + gap_x;
		rect.y = y;
		rect.width = gap_width;
		rect.height = 2;
		break;
	case GTK_POS_BOTTOM:
		rect.x = x + gap_x;
		rect.y = y + height - 2;
		rect.width = gap_width;
		rect.height = 2;
		break;
	case GTK_POS_LEFT:
		rect.x = x;
		rect.y = y + gap_x;
		rect.width = 2;
		rect.height = gap_width;
		break;
	case GTK_POS_RIGHT:
		rect.x = x + width - 2;
		rect.y = y + gap_x;
		rect.width = 2;
		rect.height = gap_width;
		break;
	}

	gtk_style_apply_default_background (style, window,
					    widget && !GTK_WIDGET_NO_WINDOW (widget),
					    state_type, area, rect.x, rect.y,
					    rect.width, rect.height);
}

static void
draw_box_gap (GtkStyle *style,
	      GdkWindow *window,
	      GtkStateType state_type,
	      GtkShadowType shadow_type,
	      GdkRectangle *area,
	      GtkWidget *widget,
	      const gchar *detail,
	      gint x,
	      gint y, gint width, gint height, GtkPositionType gap_side, gint gap_x, gint gap_width)
{
	GdkRectangle rect;

	g_return_if_fail (style != NULL);
	g_return_if_fail (window != NULL);

	if (width == -1 && height == -1)
		gdk_window_get_size (window, &width, &height);
	else if (width == -1)
		gdk_window_get_size (window, &width, NULL);
	else if (height == -1)
		gdk_window_get_size (window, NULL, &height);

	gtk_paint_box (style, window, state_type, shadow_type, area, widget,
		       detail, x, y, width, height);

	switch (gap_side) {
	case GTK_POS_TOP:
		rect.x = x + gap_x + 2;
		rect.y = y;
		rect.width = gap_width - 4;
		rect.height = 2;
		break;
	case GTK_POS_BOTTOM:
		rect.x = x + gap_x + 2;
		rect.y = y + height - 2;
		rect.width = gap_width - 4;
		rect.height = 2;
		break;
	case GTK_POS_LEFT:
		rect.x = x;
		rect.y = y + gap_x + 2;
		rect.width = 2;
		rect.height = gap_width - 4;
		break;
	case GTK_POS_RIGHT:
		rect.x = x + width - 2;
		rect.y = y + gap_x + 2;
		rect.width = 2;
		rect.height = gap_width - 4;
		break;
	}

	gtk_style_apply_default_background (style, window,
					    widget && !GTK_WIDGET_NO_WINDOW (widget),
					    state_type, area,
					    rect.x, rect.y, rect.width, rect.height);
}

static void
draw_extension (GtkStyle *style,
		GdkWindow *window,
		GtkStateType state_type,
		GtkShadowType shadow_type,
		GdkRectangle *area,
		GtkWidget *widget,
		const gchar *detail,
		gint x, gint y, gint width, gint height, GtkPositionType gap_side)
{
	GdkGC *gc1 = NULL;
	GdkGC *gc2 = NULL;
	GdkGC *gc3 = NULL;
	GdkGC *gc4 = NULL;

	g_return_if_fail (style != NULL);
	g_return_if_fail (window != NULL);

	if ((width == -1) && (height == -1))
		gdk_window_get_size (window, &width, &height);
	else if (width == -1)
		gdk_window_get_size (window, &width, NULL);
	else if (height == -1)
		gdk_window_get_size (window, NULL, &height);


	gtk_style_apply_default_background (style, window,
					    widget && !GTK_WIDGET_NO_WINDOW (widget),
					    GTK_STATE_NORMAL, area, x, y, width, height);

	switch (shadow_type) {
	case GTK_SHADOW_NONE:
		return;
	case GTK_SHADOW_IN:
		gc1 = style->bg_gc[state_type];
		gc2 = style->black_gc;
		gc3 = style->dark_gc[state_type];
		gc4 = style->light_gc[state_type];
		break;
	case GTK_SHADOW_ETCHED_IN:
		gc1 = style->dark_gc[state_type];
		gc2 = style->light_gc[state_type];
		gc3 = style->dark_gc[state_type];
		gc4 = style->light_gc[state_type];
		break;
	case GTK_SHADOW_OUT:
		gc1 = style->dark_gc[state_type];
		gc2 = style->bg_gc[state_type];
		gc3 = style->light_gc[state_type];
		gc4 = style->black_gc;
		break;
	case GTK_SHADOW_ETCHED_OUT:
		gc1 = style->light_gc[state_type];
		gc2 = style->dark_gc[state_type];
		gc3 = style->light_gc[state_type];
		gc4 = style->dark_gc[state_type];
		break;
	}

	if (area) {
		gdk_gc_set_clip_rectangle (gc1, area);
		gdk_gc_set_clip_rectangle (gc2, area);
		gdk_gc_set_clip_rectangle (gc3, area);
		gdk_gc_set_clip_rectangle (gc4, area);
	}

	switch (gap_side) {
	case GTK_POS_TOP:
		gtk_style_apply_default_background (style, window,
						    widget && !GTK_WIDGET_NO_WINDOW(widget),
						    state_type, area, x + style->xthickness,
						    y, width - (2 * style->xthickness),
						    height - (style->ythickness));
		y -= 1;
		/* Bottom */
		gdk_draw_line (window, gc1, x + 1, y + height, x + width - 2, y + height);
		gdk_draw_line (window, gc2, x + 2, y + height - 1, x + width - 2, y + height - 1);
		/* Left */
		gdk_draw_line (window, gc1, x, y, x, y + height - 1);
		gdk_draw_line (window, gc3, x + 1, y - 1, x + 1, y + height - 1);
		/* Right */
		gdk_draw_line (window, gc1, x + width - 1, y - 1, x + width - 1, y + height - 1);
		gdk_draw_line (window, gc4, x + width - 2, y, x + width - 2, y + height - 1);
		break;
	case GTK_POS_BOTTOM:
		gtk_style_apply_default_background (style, window,
						    widget && !GTK_WIDGET_NO_WINDOW (widget),
						    state_type, area, x + style->xthickness,
						    y + style->ythickness, width - (2 * style->xthickness),
						    height - (style->ythickness));
		/* Top */
		gdk_draw_line (window, gc1, x + 1, y, x + width - 2, y);
		gdk_draw_line (window, gc2, x + 1, y + 1, x + width - 2, y + 1);
		/* Left */
		gdk_draw_line (window, gc1, x, y + 1, x, y + height);
		gdk_draw_line (window, gc3, x + 1, y + 1, x + 1, y + height);
		/* Right */
		gdk_draw_line (window, gc1, x + width - 1, y + 1, x + width - 1, y + height);
		gdk_draw_line (window, gc4, x + width - 2, y + 1, x + width - 2, y + height);
		break;
	case GTK_POS_LEFT:
		gtk_style_apply_default_background (style, window,
						    widget && !GTK_WIDGET_NO_WINDOW (widget),
						    state_type, area, x, y + style->ythickness,
						    width - (style->xthickness),
						    height - (2 *style->ythickness));
		x -= 1;
		/* Top */
		gdk_draw_line (window, gc1, x, y, x + width - 1, y);
		gdk_draw_line (window, gc3, x - 1, y + 1, x + width - 1, y + 1);
		gdk_draw_line (window, gc2, x, y + 2, x + width - 2, y + 2);
		/* Right */
		gdk_draw_line (window, gc1, x + width, y + 1, x + width, y + height - 1);
		gdk_draw_line (window, gc2, x + width - 1, y + 2, x + width - 1, y + height - 2);
		/* Bottom */
		gdk_draw_line (window, gc2, x - 1, y + height - 2, x + width - 1, y + height - 2);
		gdk_draw_line (window, gc4, x - 1, y + height - 1, x + width - 1, y + height - 1);
		gdk_draw_line (window, gc1, x, y + height, x + width - 1, y + height);
		break;
	case GTK_POS_RIGHT:
		gtk_style_apply_default_background (style, window,
						    widget && !GTK_WIDGET_NO_WINDOW (widget),
						    state_type, area, x + style->xthickness,
						    y + style->ythickness,
						    width - (style->xthickness),
						    height - (2 *style->ythickness));
		/* Top */
		gdk_draw_line (window, gc1, x + 1, y, x + width - 1, y);
		gdk_draw_line (window, gc3, x + 1, y + 1, x + width, y + 1);
		gdk_draw_line (window, gc2, x + 1, y + 2, x + width, y + 2);
		/* Left */
		gdk_draw_line (window, gc1, x, y + 1, x, y + height - 1);
		gdk_draw_line (window, gc2, x + 1, y + 2, x + 1, y + height - 1);
		/* Bottom */
		gdk_draw_line (window, gc2, x + 1, y + height - 2, x + width, y + height - 2);
		gdk_draw_line (window, gc4, x + 1, y + height - 1, x + width, y + height - 1);
		gdk_draw_line (window, gc1, x + 1, y + height, x + width, y + height);
		break;
	}

	if (area) {
		gdk_gc_set_clip_rectangle (gc1, NULL);
		gdk_gc_set_clip_rectangle (gc2, NULL);
		gdk_gc_set_clip_rectangle (gc3, NULL);
		gdk_gc_set_clip_rectangle (gc4, NULL);
	}
}

static void
draw_focus (GtkStyle *style,
	    GdkWindow *window,
	    GtkStateType state_type,
	    GdkRectangle *area,
	    GtkWidget *widget, const gchar *detail, gint x, gint y, gint width, gint height)
{
	g_return_if_fail (style != NULL);
	g_return_if_fail (window != NULL);

	if ((width == -1) && (height == -1)) {
		gdk_window_get_size (window, &width, &height);
		width -= 1;
		height -= 1;
	} else if (width == -1) {
		gdk_window_get_size (window, &width, NULL);
		width -= 1;
	} else if (height == -1) {
		gdk_window_get_size (window, NULL, &height);
		height -= 1;
	}
	if (area) {
		gdk_gc_set_clip_rectangle (style->bg_gc[GTK_STATE_PRELIGHT], area);
	}

	gdk_draw_rectangle (window, style->bg_gc[GTK_STATE_PRELIGHT], FALSE,
			    x, y, width - 2, height - 2);

	if (area) {
		gdk_gc_set_clip_rectangle (style->bg_gc[GTK_STATE_PRELIGHT], NULL);
	}
}

static void
draw_slider (GtkStyle *style,
	     GdkWindow *window,
	     GtkStateType state_type,
	     GtkShadowType shadow_type,
	     GdkRectangle *area,
	     GtkWidget *widget,
	     const gchar *detail,
	     gint x, gint y, gint width, gint height, GtkOrientation orientation)
{
	g_return_if_fail (style != NULL);
	g_return_if_fail (window != NULL);

	if ((width == -1) && (height == -1))
		gdk_window_get_size (window, &width, &height);
	else if (width == -1)
		gdk_window_get_size (window, &width, NULL);
	else if (height == -1)
		gdk_window_get_size (window, NULL, &height);

	gtk_draw_box (style, window, state_type, shadow_type, x, y, width, height);

	if (orientation == GTK_ORIENTATION_HORIZONTAL)
		draw_vline (style, window, state_type, area, widget, detail,
			    style->ythickness, height - style->ythickness - 1, (width / 2) - 1);
	else
		draw_hline (style, window, state_type, area, widget, detail,
			    style->xthickness, width - style->xthickness - 1, (height / 2) - 1);
}

static void
draw_handle (GtkStyle *style,
	     GdkWindow *window,
	     GtkStateType state_type,
	     GtkShadowType shadow_type,
	     GdkRectangle *area,
	     GtkWidget *widget,
	     const gchar *detail,
	     gint x, gint y, gint width, gint height, GtkOrientation orientation)
{
	g_return_if_fail (style != NULL);
	g_return_if_fail (window != NULL);

	if ((width == -1) && (height == -1))
		gdk_window_get_size (window, &width, &height);
	else if (width == -1)
		gdk_window_get_size (window, &width, NULL);
	else if (height == -1)
		gdk_window_get_size (window, NULL, &height);

	gtk_paint_box (style, window, state_type, shadow_type, area, widget,
		       detail, x, y, width, height);
	if (orientation == GTK_ORIENTATION_HORIZONTAL) {
		draw_vline (style, window, state_type, area, widget,
			    detail, style->ythickness,
			    height - style->ythickness - 1, (width / 2) - 4);
		draw_vline (style, window, state_type, area, widget,
			    detail, style->ythickness,
			    height - style->ythickness - 1, (width / 2) - 1);
		draw_vline (style, window, state_type, area, widget,
			    detail, style->ythickness,
			    height - style->ythickness - 1, (width / 2) + 2);
	} else {
		draw_hline (style, window, state_type, area, widget,
			    detail, style->xthickness,
			    width - style->xthickness - 1, (height / 2) - 4);
		draw_hline (style, window, state_type, area, widget,
			    detail, style->xthickness,
			    width - style->xthickness - 1, (height / 2) - 1);
		draw_hline (style, window, state_type, area, widget,
			    detail, style->xthickness,
			    width - style->xthickness - 1, (height / 2) + 2);
	}
}

/* Do some magic with the colors */

static void
classique_engine_style_shade (GdkColor *a, GdkColor *b, gdouble k)
{
	gdouble red;
	gdouble green;
	gdouble blue;

	red = (gdouble) a->red / 65535.0;
	green = (gdouble) a->green / 65535.0;
	blue = (gdouble) a->blue / 65535.0;

	rgb_to_hls (&red, &green, &blue);

	green *= k;
	if (green > 1.0)
		green = 1.0;
	else if (green < 0.0)
		green = 0.0;

	blue *= k;
	if (blue > 1.0)
		blue = 1.0;
	else if (blue < 0.0)
		blue = 0.0;

	hls_to_rgb (&red, &green, &blue);

	b->red = red *65535.0;
	b->green = green *65535.0;
	b->blue = blue *65535.0;
}

static void
hls_to_rgb (gdouble *h, gdouble *l, gdouble *s)
{
	gdouble hue;
	gdouble lightness;
	gdouble saturation;
	gdouble m1, m2;
	gdouble r, g, b;

	lightness = *l;
	saturation = *s;

	if (lightness <= 0.5)
		m2 = lightness *(1 + saturation);
	else
		m2 = lightness + saturation - lightness *saturation;
	m1 = 2 *lightness - m2;

	if (saturation == 0) {
		*h = lightness;
		*l = lightness;
		*s = lightness;
	} else {
		hue = *h + 120;
		while (hue > 360)
			hue -= 360;
		while (hue < 0)
			hue += 360;

		if (hue < 60)
			r = m1 + (m2 - m1) *hue / 60;
		else if (hue < 180)
			r = m2;
		else if (hue < 240)
			r = m1 + (m2 - m1) *(240 - hue) / 60;
		else
			r = m1;

		hue = *h;
		while (hue > 360)
			hue -= 360;
		while (hue < 0)
			hue += 360;

		if (hue < 60)
			g = m1 + (m2 - m1) *hue / 60;
		else if (hue < 180)
			g = m2;
		else if (hue < 240)
			g = m1 + (m2 - m1) *(240 - hue) / 60;
		else
			g = m1;

		hue = *h - 120;
		while (hue > 360)
			hue -= 360;
		while (hue < 0)
			hue += 360;

		if (hue < 60)
			b = m1 + (m2 - m1) *hue / 60;
		else if (hue < 180)
			b = m2;
		else if (hue < 240)
			b = m1 + (m2 - m1) *(240 - hue) / 60;
		else
			b = m1;

		*h = r;
		*l = g;
		*s = b;
	}
}

static void
rgb_to_hls (gdouble *r, gdouble *g, gdouble *b)
{
	gdouble min;
	gdouble max;
	gdouble red;
	gdouble green;
	gdouble blue;
	gdouble h, l, s;
	gdouble delta;

	red = *r;
	green = *g;
	blue = *b;

	if (red > green) {
		if (red > blue)
			max = red;
		else
			max = blue;

		if (green < blue)
			min = green;
		else
			min = blue;
	} else {
		if (green > blue)
			max = green;
		else
			max = blue;

		if (red < blue)
			min = red;
		else
			min = blue;
	}

	l = (max + min) / 2;
	s = 0;
	h = 0;

	if (max != min) {
		if (l <= 0.5)
			s = (max - min) / (max + min);
		else
			s = (max - min) / (2 - max - min);

		delta = max - min;
		if (red == max)
			h = (green - blue) / delta;
		else if (green == max)
			h = 2 + (blue - red) / delta;
		else if (blue == max)
			h = 4 + (red - green) / delta;

		h *= 60;
		if (h < 0.0)
			h += 360;
	}

	*r = h;
	*g = l;
	*b = s;
}
